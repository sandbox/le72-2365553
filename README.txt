CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation


INTRODUCTION
------------
This module allows to post to Linked groups.
Depends on linkedin module.
User should be authorized on LinkedIn.
Linkedin profile integration (sub module of Linkedin integration)
should be enabled.
Synchronise LinkedIn user profiles with a Drupal user profile.
Posts go using current user's OAuth token,
so user can post only to groups where he/she is a member.


REQUIREMENTS
------------
 * LinkedIn Integration (https://www.drupal.org/project/linkedin)
 * cURL enabled


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Setup Base settings of
 "LinkedIn integration" module (admin/config/services/linkedin)
 * IMPORTANT!!! Apply patch
  https://www.drupal.org/files/linkedin-auto_login_d7-1542130-45.patch
   the important part is:
   - $url = $base_url . "/requestToken";
   + $url = $base_url . "/requestToken?scope=r_fullprofile+r_emailaddress+r_contactinfo";

   you should also add 'rw_groups' permission, so replacement actually become
   + $url = $base_url . "/requestToken?scope=r_fullprofile+r_emailaddress+r_contactinfo+rw_groups";

 * For requests from non secure sites (non https)
 set CURLOPT_SSL_VERIFYPEER to 0
 in _linkedin_http_request() function around line 340 of linkedin.inc file
 * Add 'Linkedin post to group form' block to some region
